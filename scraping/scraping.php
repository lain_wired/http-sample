<?php
    $uri = 'https://example.com/'; #取得するURL
    $html = file_get_contents($uri);

    #そのまま表示
    echo $html;

    require_once 'vendor/autoload.php';
    $doc = phpQuery::newDocument($html);
    #特定タグの取り出し
    echo $doc['title'];

    #CSSセレクタの使用
    echo $doc->find('.class');

    #属性の取り出し
    foreach($doc->find('.class') as $element){
        echo pq($element)->attr('id').PHP_EOL;
    }
?>
