# coding: utf-8
import requests
from bs4 import BeautifulSoup
uri = 'https://example.com/' #取得するURL
html = requests.get(uri)

#そのまま表示
print(html.text)

soup = BeautifulSoup(html.text, 'html.parser')

#特定タグの取り出し
print(soup.find('title').string)
for element in soup.find_all('p'):
    print(element)

#指定要素の取り出し
for element in soup.find_all('p', class_='class'):
    print(element)
    #そのタグの属性の取り出し
    print(element['id'])
