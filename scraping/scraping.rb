# coding: utf-8
require 'open-uri'
require 'nokogiri'
uri = 'https://example.com/' #取得するURL
URI.open(uri)

#そのまま表示
puts html.read

doc = Nokogiri::HTML(html)

#特定タグの取り出し
puts doc.title

#CSSセレクタの使用
doc.css('.class').each do |element|
    puts element
    #そのタグの属性の取り出し
    puts element['id']
end
