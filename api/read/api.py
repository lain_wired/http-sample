# coding: utf-8
import requests

uri = 'https://example.com/' #取得するURL
response = requests.get(uri)

#JSONをそのまま表示
print(response.json())

#ループ出力
for item in response.json():
    print(item)
    #値の出力
    print(item['name'], item['value'])
