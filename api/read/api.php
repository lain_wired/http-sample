<?php
    $uri = 'https://example.com/'; #取得するURL
    $response = file_get_contents($uri);

    #JSONをそのまま表示
    echo $response;

    #連想配列に変換
    $jsonObject = json_decode($response, true);
    print_r($jsonObject);

    #ループ出力
    foreach ($jsonObject as $item){
        echo implode(',', $item) . PHP_EOL;
    }
?>