# coding: utf-8
require 'open-uri'
require 'json'
require 'net/http'
require 'uri'

uri = 'https://example.com/' #取得するURL
response = URI.open(uri)

#JSONをそのまま表示
puts response.read

#JSONオブジェクトに変換
jsonObject = JSON.load(response)
puts jsonObject

#ループ出力
jsonObject.each do |item|
    puts item
    #値の出力
    puts "#{item['name']}, #{item['value']}"
end

#最初からJSONにする場合
getUri = URI.parse('https://example.com/')
get_response = Net::HTTP.get(getUri)
puts get_response
